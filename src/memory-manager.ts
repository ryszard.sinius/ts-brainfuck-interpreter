export class MemoryManager {
    private currentCellIndex = 0;

    private cells: number[] = [0];

    public nextCell(): void {
        this.cells.push(0);
        this.currentCellIndex++;
    }

    public previousCell(): void {
        if (this.currentCellIndex === 0) {
            throw new Error('Error! Memory overflow!');
        } else {
            this.currentCellIndex--;
        }
    }

    public incrementCellValue(): void {
        (this.cells[this.currentCellIndex] >= 255) ?
            this.cells[this.currentCellIndex] = 0 :
            this.cells[this.currentCellIndex]++;
    }

    public decrementCellValue(): void {
        (this.cells[this.currentCellIndex] <= 0) ?
            this.cells[this.currentCellIndex] = 255 :
            this.cells[this.currentCellIndex]--;
    }

    public getCellValue(): number {
        return this.cells[this.currentCellIndex];
    }

    public cellValueToCharacter(): string {
        return String.fromCharCode(this.cells[this.currentCellIndex]);
    }

    public cellValueFromCharacter(char: string): void {
        this.cells[this.currentCellIndex] = (char?.length === 0 ?? true) ? 0 : char.charCodeAt(0);
    }
}