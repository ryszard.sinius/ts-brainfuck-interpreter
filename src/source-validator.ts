export class SourceValidator {
    constructor(private readonly source: string) {
        if (this.source === undefined) {
            this.source = '';
        }
    }

    public isSourceValid(): boolean {
        return this.bracketsValid();
    }

    private bracketsValid(): boolean {
        const bracketStack: string[] = [];
        let bracketCounter = 0;

        for (const char of this.source) {
            if (char === '[') {
                bracketCounter++;
                bracketStack.push('[');
            } else if (char === ']') {
                bracketCounter--;
                const topBracket = bracketStack.pop();

                if (topBracket !== '[') {
                    return false;
                }
            }
        }

        return bracketCounter === 0;
    }
}