import { Loop } from './loop';
import { SourceValidator } from './source-validator';
import { MemoryManager } from './memory-manager';
import { Stdout } from './stdout';
import { Stdin, StdinSourceFn } from './stdin';

export class BrainfuckInterpreter {
    private sourceIndex = 0;

    private loops: Map<number, Loop> = new Map();

    private sourceValidator: SourceValidator;

    private readonly memoryManager: MemoryManager;

    private readonly stdout: Stdout;

    private stdin: Stdin;

    constructor(private readonly source: string, stdinSourceFn?: StdinSourceFn) {
        if (this.source === undefined) {
            this.source = '';
        }

        this.sourceValidator = new SourceValidator(source);
        this.memoryManager = new MemoryManager();
        this.stdout = new Stdout(this.memoryManager);
        this.stdin = new Stdin(this.memoryManager, stdinSourceFn);
    }

    public async run(): Promise<Stdout> {
        this.stdout.clear();

        if (this.checkSyntax()) {
            this.buildLoops();
            await this.interpretSource();
            return this.stdout;
        } else {
            throw new Error('Error! Invalid syntax!');
        }
    }

    public checkSyntax(): boolean {
        return this.sourceValidator.isSourceValid();
    }

    private buildLoops(): void {
        this.loops.clear();
        this.sourceIndex = 0;
        const helperLoopStack: Loop[] = [];

        while (this.sourceIndex < this.source?.length) {
            if (this.source[this.sourceIndex] === '[') {
                helperLoopStack.push(new Loop(this.sourceIndex));
            } else if (this.source[this.sourceIndex] === ']') {
                const notClosedLoop = helperLoopStack.pop();
                notClosedLoop.endIndex = this.sourceIndex;
                this.loops.set(this.sourceIndex, notClosedLoop);
            }

            this.sourceIndex++;
        }
    }

    private async interpretSource(): Promise<void> {
        this.sourceIndex = -1;

        while (this.sourceIndex < this.source?.length) {
            this.sourceIndex++;
            const char = this.source[this.sourceIndex];
            await this.interpretOperator(char);
        }
    }

    private async interpretOperator(char: string): Promise<void> {
        switch (char) {
            case '>':
                this.memoryManager.nextCell();
                break;
            case '<':
                this.memoryManager.previousCell();
                break;
            case ']':
                this.loopIfCellNotZero();
                break;
            case '+':
                this.memoryManager.incrementCellValue();
                break;
            case '-':
                this.memoryManager.decrementCellValue();
                break;
            case '.':
                this.stdout.putChar();
                break;
            case ',':
                await this.stdin.getChar();
                break;
        }
    }

    private loopIfCellNotZero(): void {
        if (this.memoryManager.getCellValue() > 0) {
            const startIndex = this.loops.get(this.sourceIndex).startIndex;

            if (startIndex === undefined || startIndex === null) {
                throw new Error('Error! Loop without start index at position: ' + this.sourceIndex);
            }

            this.sourceIndex = startIndex;
        }
    }
}