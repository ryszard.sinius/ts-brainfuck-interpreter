import { BrainfuckInterpreter } from './brainfuck-interpreter';
import { Stdout } from './stdout';

const interpreter: BrainfuckInterpreter = new BrainfuckInterpreter(prompt('Program code:'),
    async () => {
        return '5';
    });

interpreter.run().then((stdout: Stdout) => {
    const start = Date.now();

    if (stdout.value.length) {
        console.log(stdout.value);
    }

    console.log('Finished in ' + (Date.now() - start) + 'ms');
});