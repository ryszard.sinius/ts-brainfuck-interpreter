import { MemoryManager } from './memory-manager';

export class Stdout {
    private _value: string = '';

    constructor(private memoryManager: MemoryManager) {
    }

    public putChar(): void {
        this._value += this.memoryManager.cellValueToCharacter();
    }

    public clear(): void {
        this._value = '';
    }

    public get value(): string {
        return this._value;
    }
}