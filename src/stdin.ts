import { MemoryManager } from './memory-manager';

export type StdinSourceFn = () => Promise<string>;

export class Stdin {
    constructor(private memoryManager: MemoryManager,
                private stdinSourceFn: StdinSourceFn) {
    }

    public async getChar(): Promise<void> {
        let char;

        if (this.stdinSourceFn === undefined || this.stdinSourceFn === null) {
            console.info('BrainfuckInterpreter: Stdin source function was not provided. Cell filled with a value of 0.');
            char = '0';
        } else {
            char = await this.stdinSourceFn();
        }

        this.memoryManager.cellValueFromCharacter(char);
    }
}