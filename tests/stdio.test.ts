import { MemoryManager } from '../src/memory-manager';
import { Stdin } from '../src/stdin';
import { BrainfuckInterpreter } from '../src/brainfuck-interpreter';
import { Stdout } from '../src/stdout';

describe('Should set cell value to ASCII value of character being returned from StdinSourceFn', function () {
    it('Set cell value from char', async function () {
        const memoryManager = new MemoryManager();
        const stdin: Stdin = new Stdin(memoryManager, async () => '5');
        return stdin.getChar().then(() => {
            expect(memoryManager.cellValueToCharacter()).toBe('5');
        });
    })
});

describe('Should read stdin and write it to stdout', function () {
    it('Set stdin and write to stdout', async function () {
        const interpreter = new BrainfuckInterpreter(',.', async () => '5');
        return interpreter.run().then((stdout: Stdout) => {
            expect(stdout.value).toBe('5');
        });
    })
});

describe('Should print "Hello World!"', function () {
    it('Print hello world', async function () {
        const interpreter = new BrainfuckInterpreter('++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.' +
            '+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.');
        return interpreter.run().then((stdout: Stdout) => {
            expect(stdout.value).toBe('Hello World!\n');
        });
    })
});